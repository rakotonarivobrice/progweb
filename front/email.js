const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const regexName = /^[\S]{1,50}$/

document.querySelector('#contact form button')
        .addEventListener('click', function(event){
           event.preventDefault()
           let mailInput = document.getElementById('mail')
           let nameInput = document.getElementById('name')
           let messageInput = document.getElementById('message')
           let errorBool = false

           if (nameInput.value.match(regexName) === null){
                nameInput.classList.add('error')
                errorBool = true
           }

           nameInput.addEventListener('input', function(){
               if (this.value.match(regexName) === null){
                    this.classList.add('error')
               }

               else {
                    this.classList.remove('error')
               }

           })

           if (mailInput.value.match(regexMail) === null){
                mailInput.classList.add('error')
                errorBool = true
           } 

           mailInput.addEventListener('input', function(){
               if (this.value.match(regexMail) === null){
                    this.classList.add('error')
               }

               else {
                    this.classList.remove('error')
               }
           })

           if (messageInput.value === ''){
                messageInput.classList.add('error')
                errorBool = true
           }

           messageInput.addEventListener('input', function(){
               if (this.value === ''){
                    this.classList.add('error')
               }

               else {
                    this.classList.remove('error')
               }
           })

           if(!errorBool) {
               document.querySelector('form').submit()
           }
        })