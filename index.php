<?php include './back/email.php';?>
<?php include './back/traduction.php';?>
<?php include './back/gestionBDD.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./front/css/style.css" />
    <link rel="stylesheet" href="./front/css/slides-style.css" />
    <link rel="stylesheet" href="./front/css/nav-point.css" />
    <title>LoGo</title>
</head>

<body>
    <?php include './front/acceuil.php';?>
    <div>
        <section class="presentation"> 
        <nav>
            <a href="#"><div class="point-navigationA"></div></a>
            <a href="#presentation"><div class="point-navigationB"></div></a>
            <a href="#contact"><div class="point-navigationC"></div></a>
        </nav>
            <div id="presentation">
                <h1><?php echo $titre[$langue]?></h1>
                <p><?php echo $desciptionSite[$langue]?></p>

                <section class="fondateurs">
                    <img src="./front/image/moi.jpg" alt="une image de moi" />
                    <div><?php echo $desciptionSite[$langue]?></div>
                </section>               
            </div>
        </section>

        <section class="contact">
            <div id="contact">
                <form method="post">
                    <section class="style">
 
                    </section>

                    <section class="formulaire">
                        <h3 class="titre"><?php echo $contact[$langue]?></h3>

                        <div>
                            <label for="Name"></label>
                            <input type="text" placeholder="<?php echo $nomTrad[$langue]?>" id="name" name="name" />
                        </div>

                        <div>
                            <label for="mail"></label>
                            <input type="text" placeholder="<?php echo $mailTrad[$langue]?>" id="mail" name="mail" />
                        </div>

                        <div>
                            <label for="message"></label> 
                            <textarea placeholder="<?php echo $messageTrad[$langue]?>" id="message" name="message" cols="30" rows="10"></textarea>
                        </div>

                        <button type="submit"><?php echo $soumettre[$langue]?></button>
                    </section>
                </form>

                <?php 
                    $mails = $pdo -> query(
                        "SELECT email FROM question"
                    )-> fetchAll();

                    foreach($mails as $maile){
                ?>
                       <p><?php echo $mails?></p>
                <?php
                    }
                ?>
            </div>
        </section>
    </div>

<script src="./front/email.js"></script>
</body>
</html>