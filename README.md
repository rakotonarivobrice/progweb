Le site n'est pas complet.
Il y manque notemment les photos avec AJAX, faute de temps.
A part ca malgré le fait qu'il ait l'air vide, il possède plus ou moins tout le reste.

Les fonctionnalités implantés (par rapport a ce qui est demandé):
- les balises/attributs HTML obligatoires
- respect de l'archtitecure basique d'un site
- utilisation de selecteurs CSS appropriés
- utilisation de balises HTML nommés
- respect des règles d'accessibilité
- responsive (au mieux, mais pas idéal car l'écriture devient peut etre trop petite?
- utilisation de balises cohérentes pour le formulaire
- formulaire fonctionnel 
- vérification des données avant envoie, affichage des erreurs de remplissage
- envoi formulaire après vérification des données
- utilisation d'une base de donnée avec PDO
- création d'une table
- vérification des données avant insertion
- faille XSS
- extraction des données 
- système de traduction
- utilisation d'inclusions pour les éléments redontants
- BDD dans le README
- utilisation d'objets PHP pour la gestion de la BDD
 
Le site fonctionne en One page. Attention, on ne peut pas scroll. Cette 
fonctionnalité est voulue pour correspondre au mieux a l'idée du site,
pour une navigation zen. Pour naviguer, il faut soit cliquer sur les liens 
disponibles a l'acceuil, soit avec les petits boutons a droite de l'écran.

Il y a 3 sections : la section 1 correspond a l'accueil. Si on passe la souris
sur le cercle, il y a une petite animation. Pareil pour le texte dans le cercle.
les liens en haut a gauche sont pour naviguer ou traduire le site.

section 2 : juste une simple section A propos, pas grand chose a rajouter.

section 3 : un formulaire de contact qui ne s'envoie que lorsque les 
informations dans les champs sont valides. Border rouge si les champs 
sont faux. 2regex sont utilisés, une pour le nom et une autre pour le mail.
Il envoie les données en back a mail.php pour pouvoir envoyer
le message au Webmaster, mais il envoie également les données a gestionBDD.php
pour pouvoir faire une base de donnée en mysql.

Une utilisation que je pourrai en faire c'est par exemple faire 
une statistique plus tard des questions les plus fréquentes avec une recherche
de mot clé par exemple.

La structure utilisée pour la BDD est simple. 
	
une table question qui:	 
- comprend un id(un entier) comme clé primaire qui s'autoincrémente	 
- comprend un champ nom(char(50))	
- un champ email(char50)) 
- un message non vide

On utilise try() catch() pour gérer les erreurs, 
on fait attention a la faille XSS et on s'assure que les éléments soit bien
de type char avec bindValue()

Une traduction dynamique du site a été faite,de l'anglais en français ou 
inversement via PHP, que l'on peut retrouver dans traduction.php.

	
