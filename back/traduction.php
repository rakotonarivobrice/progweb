<?php
$langue= 0;
    if(isset($_GET["lang"]))
        $langue= 1;

    $titre = array("LoGo, ton réseau universtaire à l'international",
                    "LoGo, your international university network");

    $slogan = array("La solution pour les étudiants",
                     "The student solution");
    
    $aPropos = array("A propos",
                      "About me");

    $langFR = array("Français",
                     "French");

    $langENG = array("Anglais",
                      "English");
                      
    $contact = array("Contactez moi",
                      "Contact me");
     
    $mailTrad = array("Votre Email",
                       "Your Email");
 
    $nomTrad = array("Votre Nom",
                      "Your Name");

    $messageTrad = array("Tapez votre message...",
                          "Write your message...");
    
    $soumettre = array("Soumettre",
                       "Submit");

    $desciptionSite = array("Née d'une directive étudiante, LoGo est un dispositif ayant pour but de créer
                            un réseau de mise en relation entre écoles françaises et écoles étrangères. 
                            Beaucoup d'étudiants rêvent de voyager tout en continuant leurs études et
                            ce n'est pas toujours évident ! C'est pourquoi je mise sur la simplicité
                            d'utilisation, pour apporter un confort lors de la recherche ainsi qu'un
                            accompagnement plus humain dans les démarches tout en évitant les informations
                            superflux pour garder l'essentiel a vu d'oeil.",

                            "Born from a student directive, LoGo is a device aimed
                             for creating a network between French schools 
                             and foreign schools. Many students dream of traveling while
                             continuing their studies and it is not always easy! This is why 
                             I focus on ease of use, to provide comfort during research as well as more human
                             support in the procedures while avoiding superfluous information to keep the 
                             essentials on your mind.");
    
    $desciptionMoi = array("Etudiant en informatique a l'université de Strasbourg, l'intérêt de ce 
                            site m'est venu a l'esprit naturellement car je suis quelqu'un qui aime 
                            travailler pour des choses plus humaines. Je préfère laisser une part
                            importante a l'interaction et au calme plutôt qu'un rythme parfois trop
                            soutenu qui apporte stress, anxiété et erreurs. Cela ne signifie en aucun 
                            cas que j'aime le laisser aller ! J'estime qu'il ne faut pas tomber dans 
                            dans un coté comme dans l'autre.",
                            
                            "Student in computer science at the University of Strasbourg, the interest of this 
                            site came to my mind naturally because I am someone who likes to work for more human
                            things. I prefer having an important part of interaction while being calm rather 
                            than a sustained rhythm which brings stress, anxiety and errors. That doesn't
                            mean I like to chill only! I believe that we should not fall into one side or an other.");