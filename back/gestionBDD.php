<?php

$name = htmlspecialchars($_POST['name']);
$mail = htmlspecialchars($_POST['mail']);
$message = htmlspecialchars($_POST['message']);

if ($name && $mail && $message) {
    try {
        $pdo = new PDO("mysql:host=localhost;port=3306;dbname=new_database","brice","helloWorld");
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->query(
                'CREATE TABLE IF NOT EXISTS question( 
                id INT AUTO_INCREMENT PRIMARY KEY, 
                nom VARCHAR(50) NOT NULL,
                email VARCHAR(50) NOT NULL,
                userMessage TEXT NOT NULL
              )'
        );

        $statement = $pdo->prepare(
            "INSERT INTO question(nom, email, userMessage) VALUES(:name, :mail, :message)"
        );

        $statement -> bindValue('nom',$name, PDO::PARAM_STR);
        $statement -> bindValue('email',$mail, PDO::PARAM_STR);
        $statement -> bindValue('userMessage',$message, PDO::PARAM_STR);
        $statement -> execute();

    } catch (PDOException $exception) {
           var_dump($exception);
    }
}